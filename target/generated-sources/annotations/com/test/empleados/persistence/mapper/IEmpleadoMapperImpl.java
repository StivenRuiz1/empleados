package com.test.empleados.persistence.mapper;

import com.test.empleados.domain.model.Employee;
import com.test.empleados.persistence.entity.Empleado;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-08-13T19:30:56-0500",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.13 (Oracle Corporation)"
)
@Component
public class IEmpleadoMapperImpl implements IEmpleadoMapper {

    @Autowired
    private IAreaMapper iAreaMapper;
    @Autowired
    private IPaisMapper iPaisMapper;
    @Autowired
    private ITipoIdentificacionMapper iTipoIdentificacionMapper;

    @Override
    public Employee toEmployee(Empleado empleado) {
        if ( empleado == null ) {
            return null;
        }

        Employee employee = new Employee();

        employee.setEmployeeId( empleado.getIdEmpleado() );
        employee.setSurname( empleado.getPrimerApellido() );
        employee.setSecondSurname( empleado.getSegundoApellido() );
        employee.setFirstName( empleado.getPrimerNombre() );
        employee.setOtherNames( empleado.getOtrosNombres() );
        employee.setCountryEmployment( iPaisMapper.toCountry( empleado.getPaisEmpleo() ) );
        employee.setIdentificationType( iTipoIdentificacionMapper.toIdentificationType( empleado.getTipoIdentificacion() ) );
        employee.setIdentificationNumber( empleado.getNumeroIdentificacion() );
        employee.setEmail( empleado.getCorreoElectronico() );
        employee.setAdmissionDate( empleado.getFechaIngreso() );
        employee.setArea( iAreaMapper.toArea( empleado.getAreaE() ) );
        employee.setState( empleado.getEstado() );
        employee.setRegistrationDate( empleado.getFechaRegistro() );
        employee.setModificationDate( empleado.getFechaModificacion() );

        return employee;
    }

    @Override
    public List<Employee> toEmployees(List<Empleado> empleados) {
        if ( empleados == null ) {
            return null;
        }

        List<Employee> list = new ArrayList<Employee>( empleados.size() );
        for ( Empleado empleado : empleados ) {
            list.add( toEmployee( empleado ) );
        }

        return list;
    }

    @Override
    public Empleado toEmpleado(Employee employee) {
        if ( employee == null ) {
            return null;
        }

        Empleado empleado = new Empleado();

        empleado.setIdEmpleado( employee.getEmployeeId() );
        empleado.setPrimerApellido( employee.getSurname() );
        empleado.setSegundoApellido( employee.getSecondSurname() );
        empleado.setPrimerNombre( employee.getFirstName() );
        empleado.setOtrosNombres( employee.getOtherNames() );
        empleado.setPaisEmpleo( iPaisMapper.toPais( employee.getCountryEmployment() ) );
        empleado.setTipoIdentificacion( iTipoIdentificacionMapper.toTipoIdentificacion( employee.getIdentificationType() ) );
        empleado.setNumeroIdentificacion( employee.getIdentificationNumber() );
        empleado.setCorreoElectronico( employee.getEmail() );
        empleado.setFechaIngreso( employee.getAdmissionDate() );
        empleado.setAreaE( iAreaMapper.toAreaE( employee.getArea() ) );
        empleado.setEstado( employee.getState() );
        empleado.setFechaRegistro( employee.getRegistrationDate() );
        empleado.setFechaModificacion( employee.getModificationDate() );

        return empleado;
    }

    @Override
    public List<Empleado> toEmpleados(List<Employee> employee) {
        if ( employee == null ) {
            return null;
        }

        List<Empleado> list = new ArrayList<Empleado>( employee.size() );
        for ( Employee employee1 : employee ) {
            list.add( toEmpleado( employee1 ) );
        }

        return list;
    }
}
