package com.test.empleados.persistence.mapper;

import com.test.empleados.domain.model.Area;
import com.test.empleados.persistence.entity.AreaE;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-08-13T19:30:55-0500",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.13 (Oracle Corporation)"
)
@Component
public class IAreaMapperImpl implements IAreaMapper {

    @Override
    public Area toArea(AreaE area) {
        if ( area == null ) {
            return null;
        }

        Area area1 = new Area();

        area1.setAreaId( area.getIdArea() );
        area1.setAreaName( area.getNombreArea() );

        return area1;
    }

    @Override
    public List<Area> toAreas(List<AreaE> area) {
        if ( area == null ) {
            return null;
        }

        List<Area> list = new ArrayList<Area>( area.size() );
        for ( AreaE areaE : area ) {
            list.add( toArea( areaE ) );
        }

        return list;
    }

    @Override
    public AreaE toAreaE(Area area) {
        if ( area == null ) {
            return null;
        }

        AreaE areaE = new AreaE();

        areaE.setIdArea( area.getAreaId() );
        areaE.setNombreArea( area.getAreaName() );

        return areaE;
    }

    @Override
    public List<AreaE> toAreasE(List<Area> area) {
        if ( area == null ) {
            return null;
        }

        List<AreaE> list = new ArrayList<AreaE>( area.size() );
        for ( Area area1 : area ) {
            list.add( toAreaE( area1 ) );
        }

        return list;
    }
}
