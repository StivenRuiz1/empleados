package com.test.empleados.persistence.mapper;

import com.test.empleados.domain.model.IdentificationType;
import com.test.empleados.persistence.entity.TipoIdentificacion;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-08-13T19:30:56-0500",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.13 (Oracle Corporation)"
)
@Component
public class ITipoIdentificacionMapperImpl implements ITipoIdentificacionMapper {

    @Override
    public IdentificationType toIdentificationType(TipoIdentificacion tipoIdentificacion) {
        if ( tipoIdentificacion == null ) {
            return null;
        }

        IdentificationType identificationType = new IdentificationType();

        identificationType.setIdentificationTypeId( tipoIdentificacion.getIdTipoIdentificacion() );
        identificationType.setIdentificationTypeName( tipoIdentificacion.getTipoIdentificacionNombre() );

        return identificationType;
    }

    @Override
    public List<IdentificationType> toIdentificationTypes(List<TipoIdentificacion> tipoIdentificacion) {
        if ( tipoIdentificacion == null ) {
            return null;
        }

        List<IdentificationType> list = new ArrayList<IdentificationType>( tipoIdentificacion.size() );
        for ( TipoIdentificacion tipoIdentificacion1 : tipoIdentificacion ) {
            list.add( toIdentificationType( tipoIdentificacion1 ) );
        }

        return list;
    }

    @Override
    public TipoIdentificacion toTipoIdentificacion(IdentificationType identificationType) {
        if ( identificationType == null ) {
            return null;
        }

        TipoIdentificacion tipoIdentificacion = new TipoIdentificacion();

        tipoIdentificacion.setIdTipoIdentificacion( identificationType.getIdentificationTypeId() );
        tipoIdentificacion.setTipoIdentificacionNombre( identificationType.getIdentificationTypeName() );

        return tipoIdentificacion;
    }

    @Override
    public List<TipoIdentificacion> toTiposIdentificacion(List<IdentificationType> identificationType) {
        if ( identificationType == null ) {
            return null;
        }

        List<TipoIdentificacion> list = new ArrayList<TipoIdentificacion>( identificationType.size() );
        for ( IdentificationType identificationType1 : identificationType ) {
            list.add( toTipoIdentificacion( identificationType1 ) );
        }

        return list;
    }
}
