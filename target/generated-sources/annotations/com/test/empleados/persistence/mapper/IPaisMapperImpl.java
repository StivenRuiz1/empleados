package com.test.empleados.persistence.mapper;

import com.test.empleados.domain.model.Country;
import com.test.empleados.persistence.entity.Pais;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-08-13T19:30:56-0500",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 11.0.13 (Oracle Corporation)"
)
@Component
public class IPaisMapperImpl implements IPaisMapper {

    @Override
    public Country toCountry(Pais pais) {
        if ( pais == null ) {
            return null;
        }

        Country country = new Country();

        country.setCountryId( pais.getIdPais() );
        country.setCountryName( pais.getNombrePais() );

        return country;
    }

    @Override
    public List<Country> toCountries(List<Pais> pais) {
        if ( pais == null ) {
            return null;
        }

        List<Country> list = new ArrayList<Country>( pais.size() );
        for ( Pais pais1 : pais ) {
            list.add( toCountry( pais1 ) );
        }

        return list;
    }

    @Override
    public Pais toPais(Country Country) {
        if ( Country == null ) {
            return null;
        }

        Pais pais = new Pais();

        pais.setIdPais( Country.getCountryId() );
        pais.setNombrePais( Country.getCountryName() );

        return pais;
    }

    @Override
    public List<Pais> toPaises(List<Country> Country) {
        if ( Country == null ) {
            return null;
        }

        List<Pais> list = new ArrayList<Pais>( Country.size() );
        for ( Country country : Country ) {
            list.add( toPais( country ) );
        }

        return list;
    }
}
