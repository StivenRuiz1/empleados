package com.test.empleados.persistence.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.test.empleados.domain.model.Area;
import com.test.empleados.persistence.entity.AreaE;

@Mapper(componentModel = "spring")
public interface IAreaMapper {
	@Mappings({
        @Mapping(source = "idArea", target = "areaId"),
        @Mapping(source = "nombreArea", target = "areaName"),
	})
	Area toArea(AreaE area);
	List<Area> toAreas(List<AreaE> area);
	
	@InheritInverseConfiguration
	AreaE toAreaE(Area area);
	List<AreaE> toAreasE(List<Area> area);
}
