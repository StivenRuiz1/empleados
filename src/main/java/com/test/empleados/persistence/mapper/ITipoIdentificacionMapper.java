package com.test.empleados.persistence.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.test.empleados.domain.model.IdentificationType;
import com.test.empleados.persistence.entity.TipoIdentificacion;

@Mapper(componentModel = "spring")
public interface ITipoIdentificacionMapper {
	@Mappings({
        @Mapping(source = "idTipoIdentificacion", target = "identificationTypeId"),
        @Mapping(source = "tipoIdentificacionNombre", target = "identificationTypeName"),
	})
	IdentificationType toIdentificationType(TipoIdentificacion tipoIdentificacion);
	List<IdentificationType> toIdentificationTypes(List<TipoIdentificacion> tipoIdentificacion);
	
	@InheritInverseConfiguration
	TipoIdentificacion toTipoIdentificacion(IdentificationType identificationType);
	List<TipoIdentificacion> toTiposIdentificacion(List<IdentificationType> identificationType);
}
