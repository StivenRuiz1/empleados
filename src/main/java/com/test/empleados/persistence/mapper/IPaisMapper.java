package com.test.empleados.persistence.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.test.empleados.domain.model.Country;
import com.test.empleados.persistence.entity.Pais;

@Mapper(componentModel = "spring")
public interface IPaisMapper {
	@Mappings({
        @Mapping(source = "idPais", target = "countryId"),
        @Mapping(source = "nombrePais", target = "countryName"),
	})
	Country toCountry(Pais pais);
	List<Country> toCountries(List<Pais> pais);
	
	@InheritInverseConfiguration
	Pais toPais(Country Country);
	List<Pais> toPaises(List<Country> Country);
}
