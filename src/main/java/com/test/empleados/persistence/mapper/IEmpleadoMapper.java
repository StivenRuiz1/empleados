package com.test.empleados.persistence.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import com.test.empleados.domain.model.Employee;
import com.test.empleados.persistence.entity.Empleado;

@Mapper(componentModel = "spring", uses = { IAreaMapper.class, IPaisMapper.class, ITipoIdentificacionMapper.class })
public interface IEmpleadoMapper {
	@Mappings({ 
			@Mapping(source = "idEmpleado", target = "employeeId"),
			@Mapping(source = "primerApellido", target = "surname"),
			@Mapping(source = "segundoApellido", target = "secondSurname"),
			@Mapping(source = "primerNombre", target = "firstName"),
			@Mapping(source = "otrosNombres", target = "otherNames"),
			@Mapping(source = "paisEmpleo", target = "countryEmployment"),
			@Mapping(source = "tipoIdentificacion", target = "identificationType"),
			@Mapping(source = "numeroIdentificacion", target = "identificationNumber"),
			@Mapping(source = "correoElectronico", target = "email"),
			@Mapping(source = "fechaIngreso", target = "admissionDate"), 
			@Mapping(source = "areaE", target = "area"),
			@Mapping(source = "estado", target = "state"),
			@Mapping(source = "fechaRegistro", target = "registrationDate"),
			@Mapping(source = "fechaModificacion", target = "modificationDate"),
		})
	Employee toEmployee(Empleado empleado);
	List<Employee> toEmployees(List<Empleado> empleados);

	@InheritInverseConfiguration
	Empleado toEmpleado(Employee employee);
	List<Empleado> toEmpleados(List<Employee> employee);
}
