package com.test.empleados.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name="area", schema = "actividades")
public class AreaE {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_area", nullable = false)
	private Integer idArea;
	
	@Column(name="nombre_area", nullable = false)
	private String nombreArea;
}
