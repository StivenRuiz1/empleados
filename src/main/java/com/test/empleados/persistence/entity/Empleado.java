package com.test.empleados.persistence.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name="empleado", schema = "actividades")
public class Empleado {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_empleado", nullable = false)
	private Integer idEmpleado;
	
	@Column(name="primer_apellido")
	private String primerApellido;
	
	@Column(name="segundo_apellido")
	private String segundoApellido;
	
	@Column(name="primer_nombre")
	private String primerNombre;
	
	@Column(name="otros_nombres")
	private String otrosNombres;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="pais_empleo")
	private Pais paisEmpleo;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="tipo_identificacion")
	private TipoIdentificacion tipoIdentificacion;
	
	@Column(name="numero_identificacion")
	private String numeroIdentificacion;
	
	@Column(name="correo_electronico")
	private String correoElectronico;
	
	@Column(name="fecha_ingreso")
	private LocalDate fechaIngreso;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="area")
	private AreaE areaE;
	
	@Column(name="estado")
	private String estado;

	@Column(name="fecha_registro")
	private LocalDateTime fechaRegistro;
	
	@Column(name="fecha_modificacion")
	private LocalDateTime fechaModificacion;
	
}
