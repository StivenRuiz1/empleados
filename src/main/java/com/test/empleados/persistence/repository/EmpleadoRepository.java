package com.test.empleados.persistence.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.empleados.domain.model.Employee;
import com.test.empleados.domain.model.IdentificationType;
import com.test.empleados.domain.repository.EmployeesRepository;
import com.test.empleados.persistence.CRUD.IEmpleadoCRUDRepository;
import com.test.empleados.persistence.mapper.IEmpleadoMapper;
import com.test.empleados.persistence.mapper.ITipoIdentificacionMapper;

@Repository
public class EmpleadoRepository implements EmployeesRepository{

	@Autowired
	private IEmpleadoMapper empleadoMapper;
	
	@Autowired
	private ITipoIdentificacionMapper tipoIdentificacionMapper;
	
	@Autowired
	private IEmpleadoCRUDRepository empleadoCRUDRepository;
	
	@Override
	public List<Employee> obtenerEmpleadosFiltrados(String primerNombre, 
			String otrosNombres, String apellido, String segundoApellido, 
			Integer tipoIdentificacion, String numeroIdentificacion, 
			Integer paisEmpleado, String email, String estado) {
		return empleadoMapper.toEmployees(empleadoCRUDRepository.
				getEmpleadoByFilter(primerNombre, otrosNombres, apellido, segundoApellido,
						tipoIdentificacion, numeroIdentificacion, paisEmpleado, email,
						estado));
	}

	@Override
	public Employee crearEmpleado(Employee employee) {
		return empleadoMapper.toEmployee(empleadoCRUDRepository
				.save(empleadoMapper
						.toEmpleado(employee)));
	}

	@Override
	public void eliminarEmpleado(Integer employeeId) {
		empleadoCRUDRepository.deleteById(employeeId);
		return;
	}

	@Override
	public Integer obtenerIdCorreo(String email, String domain) {
		email = email + "%";
		domain = "%" + domain;
		return Integer.parseInt(empleadoCRUDRepository.countCorreoElectronicoFiltered(email, domain).toString());
	}
	
	@Override
	public boolean existeEmpleadoPorIdentificacion(IdentificationType identificationType,
			String numberIdentification) {
		return empleadoCRUDRepository.existsByTipoIdentificacionAndNumeroIdentificacion(
				tipoIdentificacionMapper.toTipoIdentificacion(identificationType), 
				numberIdentification);
	}
	
	@Override
	public boolean existeEmpleadoPorId(Integer id) {
		return empleadoCRUDRepository.existsById(id);
	}
	
	@Override
	public Employee obtenerEmpleadoPorId(Integer id) {
		return empleadoMapper.toEmployee(empleadoCRUDRepository.findByIdEmpleado(id));
	}
	
}
