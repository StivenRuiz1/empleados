package com.test.empleados.persistence.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.empleados.domain.model.Area;
import com.test.empleados.domain.repository.AreaRepository;
import com.test.empleados.persistence.CRUD.IAreaECRUDRepository;
import com.test.empleados.persistence.mapper.IAreaMapper;

@Repository
public class AreaERepository implements AreaRepository{

	@Autowired
	private IAreaMapper areaMapper;
	
	@Autowired
	private IAreaECRUDRepository areaECRUDRepository;
	
	@Override
	public List<Area> obtenerAreas() {
		return areaMapper.toAreas(areaECRUDRepository.findAll());
	}
	
}
