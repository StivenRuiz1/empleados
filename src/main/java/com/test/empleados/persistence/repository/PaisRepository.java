package com.test.empleados.persistence.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.empleados.domain.model.Country;
import com.test.empleados.domain.repository.CountryRepository;
import com.test.empleados.persistence.CRUD.IPaisCRUDRepository;
import com.test.empleados.persistence.mapper.IPaisMapper;

@Repository
public class PaisRepository implements CountryRepository{

	@Autowired
	private IPaisMapper paisMapper;
	
	@Autowired
	private IPaisCRUDRepository paisCRUDRepository;
	
	@Override
	public List<Country> obtenerPaises() {
		return paisMapper.toCountries(paisCRUDRepository.findAll());
	}
	
}
