package com.test.empleados.persistence.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.empleados.domain.model.IdentificationType;
import com.test.empleados.domain.repository.IdentificationTypeRepository;
import com.test.empleados.persistence.CRUD.ITipoIdentificacionCRUDRepository;
import com.test.empleados.persistence.mapper.ITipoIdentificacionMapper;

@Repository
public class TipoIdentificacionRepository implements IdentificationTypeRepository{

	@Autowired
	private ITipoIdentificacionMapper tipoIdentificacionMapper;
	
	@Autowired
	private ITipoIdentificacionCRUDRepository tipoIdentificacionCRUDRepository;
	
	@Override
	public List<IdentificationType> obtenerTiposIdentificacion() {
		return tipoIdentificacionMapper.toIdentificationTypes(tipoIdentificacionCRUDRepository.findAll());
	}
	
}
