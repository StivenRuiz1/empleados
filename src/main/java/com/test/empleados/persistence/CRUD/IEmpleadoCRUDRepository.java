package com.test.empleados.persistence.CRUD;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.test.empleados.persistence.entity.Empleado;
import com.test.empleados.persistence.entity.TipoIdentificacion;

public interface IEmpleadoCRUDRepository extends JpaRepository<Empleado, Integer>{
	
	@Query(value = "SELECT COUNT(e) FROM Empleado e "
			+ "WHERE e.correoElectronico LIKE :email "
			+ "AND e.correoElectronico LIKE :domain")
	Long countCorreoElectronicoFiltered(
			@Param("email") String email,
			@Param("domain") String domain);
	
	boolean existsByTipoIdentificacionAndNumeroIdentificacion(
			TipoIdentificacion tipoIdentificacion, String numeroIdentificacion);
	
	Empleado findByIdEmpleado(Integer empleadoId);
	
	@Query(value = "SELECT e from Empleado e "
			+ "WHERE e.idEmpleado > 0"
			+ "AND (e.primerNombre = :primerNombre OR '0' = :primerNombre) "
			+ "AND (e.otrosNombres = :otrosNombres OR '0' = :otrosNombres) "
			+ "AND (e.primerApellido = :primerApellido OR '0' = :primerApellido) "
			+ "AND (e.segundoApellido = :segundoApellido OR '0' = :segundoApellido) "
			+ "AND (e.tipoIdentificacion.idTipoIdentificacion = :tipoIdentificacion OR 0 = :tipoIdentificacion) "
			+ "AND (e.numeroIdentificacion = :numeroIdentificacion OR '0' = :numeroIdentificacion) "
			+ "AND (e.paisEmpleo.idPais = :paisEmpleo OR 0 = :paisEmpleo) "
			+ "AND (e.correoElectronico = :correoElectronico OR '0' = :correoElectronico) "
			+ "AND (e.estado = :estado OR '0' = :estado) ")
	List<Empleado> getEmpleadoByFilter(
			@Param("primerNombre") String primerNombre, 
			@Param("otrosNombres") String otrosNombres, 
			@Param("primerApellido") String primerApellido, 
			@Param("segundoApellido") String segundoApellido, 
			@Param("tipoIdentificacion") Integer tipoIdentificacion,
			@Param("numeroIdentificacion") String numeroIdentificacion,
			@Param("paisEmpleo") Integer paisEmpleo,
			@Param("correoElectronico") String correoElectronico,
			@Param("estado") String estado);
}
