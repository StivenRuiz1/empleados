package com.test.empleados.persistence.CRUD;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.empleados.persistence.entity.AreaE;

public interface IAreaECRUDRepository extends JpaRepository<AreaE, Integer>{
}
