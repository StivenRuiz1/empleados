package com.test.empleados.persistence.CRUD;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.empleados.persistence.entity.TipoIdentificacion;

public interface ITipoIdentificacionCRUDRepository extends JpaRepository<TipoIdentificacion, Integer>{
}
