package com.test.empleados.persistence.CRUD;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.empleados.persistence.entity.Pais;

public interface IPaisCRUDRepository extends JpaRepository<Pais, Integer>{
}
