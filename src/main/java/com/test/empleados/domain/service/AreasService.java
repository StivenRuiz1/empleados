package com.test.empleados.domain.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.test.empleados.domain.DTO.response.ResponseDTO;
import com.test.empleados.domain.model.Area;
import com.test.empleados.domain.repository.AreaRepository;
import com.test.empleados.domain.service.utils.Utils;

@Service
public class AreasService {

	@Autowired
	private AreaRepository areaRepository;
	
	public ResponseDTO obtenerAreas() {
		ResponseDTO responseDTO;
		List<Area> area;
		
		try {
			area = areaRepository.obtenerAreas();
			
			if (!area.isEmpty()) {
				responseDTO = Utils.updateResponse(true, Optional.of(area), 
						HttpStatus.OK.value(), 1, 1, "Consulta Exitosa.");
			} else {
				responseDTO = Utils.updateResponse(false, Optional.empty(), 
						HttpStatus.NOT_FOUND.value(), 1, 1, "No se encontraron registros.");
			}
		} catch (Exception e) {
			responseDTO = Utils.updateResponse(false, Optional.empty(), 
					HttpStatus.INTERNAL_SERVER_ERROR.value(), 1, 1, e.getMessage());
		}
		return responseDTO;
	}
	
}
