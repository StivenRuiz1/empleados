package com.test.empleados.domain.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.test.empleados.domain.DTO.response.ResponseDTO;
import com.test.empleados.domain.model.Employee;
import com.test.empleados.domain.repository.EmployeesRepository;
import com.test.empleados.domain.service.utils.Utils;

@Service
public class EmployeesService {

	@Autowired
	private EmployeesRepository employeesRepository;

	public ResponseDTO obtenerEmpleados(Integer currentPage, Integer perPage, String firstName, String otherNames,
			String surname, String secondSurname, Integer identificationType, String identificationNumber,
			Integer countryEmployment, String email, String state) {
		ResponseDTO responseDTO;
		List<Employee> employees;

		try {
			employees = employeesRepository.obtenerEmpleadosFiltrados(firstName, otherNames, surname, secondSurname,
					identificationType, identificationNumber, countryEmployment, email, state);

			if (!employees.isEmpty()) {
				perPage = perPage > 10 ? 10 : perPage;
				Integer startIndex = perPage * currentPage - perPage > employees.size() ? employees.size()
						: perPage * currentPage - perPage;
				Integer endIndex = perPage * currentPage > employees.size() ? employees.size() : perPage * currentPage;
				employees = employees.subList(startIndex, endIndex);
				responseDTO = Utils.updateResponse(true, Optional.of(employees), HttpStatus.OK.value(), currentPage,
						perPage, "Consulta Exitosa.");
			} else {
				responseDTO = Utils.updateResponse(false, Optional.empty(), HttpStatus.NOT_FOUND.value(), 1, 1,
						"No se encontraron registros.");
			}
		} catch (Exception e) {
			responseDTO = Utils.updateResponse(false, Optional.empty(), HttpStatus.INTERNAL_SERVER_ERROR.value(), 1, 1,
					e.getMessage());
		}
		return responseDTO;
	}

	public ResponseDTO crearEmpleado(Employee employee) {
		ResponseDTO responseDTO;
		Employee employeeR;
		try {

			boolean employeeExist = employeesRepository.existeEmpleadoPorIdentificacion(
					employee.getIdentificationType(), employee.getIdentificationNumber());

			if (employeeExist) {
				return responseDTO = Utils.updateResponse(false, Optional.empty(), HttpStatus.OK.value(), 1, 1,
						"Ya existe un empleado con la identificacion ingresada.");
			}

			employee.setRegistrationDate(LocalDateTime.now());
			employee.setEmail(obtenerEmail(employee.getFirstName(), employee.getSurname(),
					employee.getCountryEmployment().getCountryName()));
			employee.setState("ACTIVO");

			employeeR = employeesRepository.crearEmpleado(employee);

			if (employeeR != null) {
				responseDTO = Utils.updateResponse(true, Optional.of(employeeR), HttpStatus.CREATED.value(), 1, 1,
						"Empleado Creado.");
			} else {
				responseDTO = Utils.updateResponse(false, Optional.empty(), HttpStatus.BAD_REQUEST.value(), 1, 1,
						"No se guardo el empleado.");
			}
		} catch (Exception e) {
			responseDTO = Utils.updateResponse(false, Optional.empty(), HttpStatus.INTERNAL_SERVER_ERROR.value(), 1, 1,
					e.getMessage());
		}
		return responseDTO;
	}

	public ResponseDTO actualizarEmpleado(Employee employee) {
		ResponseDTO responseDTO;
		Employee employeeR;
		try {

			Employee employeeExist = employeesRepository.obtenerEmpleadoPorId(employee.getEmployeeId());

			if (employeeExist == null) {
				return responseDTO = Utils.updateResponse(false, Optional.empty(), HttpStatus.NOT_FOUND.value(), 1, 1,
						"No existe un empleado con el id ingresado.");
			}

			employee.setRegistrationDate(employeeExist.getRegistrationDate());
			employee.setModificationDate(LocalDateTime.now());

			if (!employee.getFirstName().equals(employeeExist.getFirstName())
					|| !employee.getSurname().equals(employeeExist.getSurname())) {
				employee.setEmail(obtenerEmail(employee.getFirstName(), employee.getSurname(),
						employee.getCountryEmployment().getCountryName()));
			}

			employeeR = employeesRepository.crearEmpleado(employee);

			if (employeeR != null) {
				responseDTO = Utils.updateResponse(true, Optional.of(employeeR), HttpStatus.OK.value(), 1, 1,
						"Empleado Actualizado.");
			} else {
				responseDTO = Utils.updateResponse(false, Optional.empty(), HttpStatus.BAD_REQUEST.value(), 1, 1,
						"No se actualizo el empleado.");
			}
		} catch (Exception e) {
			responseDTO = Utils.updateResponse(false, Optional.empty(), HttpStatus.INTERNAL_SERVER_ERROR.value(), 1, 1,
					e.getMessage());
		}
		return responseDTO;
	}

	public ResponseDTO eliminarEmpleado(Integer employeeId) {
		ResponseDTO responseDTO;
		try {
			employeesRepository.eliminarEmpleado(employeeId);
			responseDTO = Utils.updateResponse(true, Optional.empty(), HttpStatus.OK.value(), 1, 1,
					"Empleado Eliminado.");

		} catch (Exception e) {
			responseDTO = Utils.updateResponse(false, Optional.empty(), HttpStatus.INTERNAL_SERVER_ERROR.value(), 1, 1,
					e.getMessage());
		}
		return responseDTO;
	}

	public String obtenerEmail(String firstName, String surname, String countryName) throws Exception {

		String email = Utils.generateEmail(firstName, surname, countryName);
		Integer emailId = 0;
		String[] emailSplitted = email.split("@");

		try {
			emailId = employeesRepository.obtenerIdCorreo(emailSplitted[0], emailSplitted[1]);
		} catch (Exception ex) {
			throw new Exception("Error al obtener el id para el correo");
		}

		if (emailSplitted[0].length() > 285) {
			Integer maxLength = 284;
			if (emailId.toString().equals("0")) {
				emailSplitted[0].substring(0, maxLength);
			} else {
				maxLength = maxLength - emailId.toString().length() - 1;
				emailSplitted[0].substring(0, maxLength);
			}
			email = emailSplitted[0] + "@" + emailSplitted[1];
		}

		if (emailId.toString().equals("0")) {
			return email;
		} else {
			emailSplitted = email.split("@");
			return emailSplitted[0] + "." + emailId + "@" + emailSplitted[1];
		}

	}
}
