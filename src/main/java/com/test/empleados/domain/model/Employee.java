package com.test.empleados.domain.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.validation.constraints.Email;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
	
	private Integer employeeId;
	
	@Pattern(regexp="[A-Z ]{1,20}",message="surname no debe contener caracteres especiales")
	private String surname;
	
	@Pattern(regexp="[A-Z ]{1,20}",message="secondSurname no debe contener caracteres especiales")
	private String secondSurname;
	
	@Pattern(regexp="[A-Z ]{1,20}",message="firstName no debe contener caracteres especiales")
	private String firstName;
	
	@Pattern(regexp="[A-Z ]{0,50}",message="otherNames no debe contener caracteres especiales")
	private String otherNames;
	
	private Country countryEmployment;
	
	private IdentificationType identificationType;
	
	
	@Pattern(regexp="[a-zA-Z0-9-]{0,20}",message="identificationNumber no debe contener caracteres especiales")
	private String identificationNumber;
	
	@Email
	@Size(max = 300)
	private String email;
	
	@Past
	private LocalDate admissionDate;
	
	private Area area;
	
	private String state;

	private LocalDateTime registrationDate;
	
	private LocalDateTime modificationDate;
}
