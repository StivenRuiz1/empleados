package com.test.empleados.domain.repository;

import java.util.List;

import com.test.empleados.domain.model.IdentificationType;

public interface IdentificationTypeRepository {
	
	List<IdentificationType> obtenerTiposIdentificacion(); 

}
