package com.test.empleados.domain.repository;

import java.util.List;

import com.test.empleados.domain.model.Area;

public interface AreaRepository {
	
	List<Area> obtenerAreas(); 

}
