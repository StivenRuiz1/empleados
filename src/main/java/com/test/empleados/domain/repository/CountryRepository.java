package com.test.empleados.domain.repository;

import java.util.List;

import com.test.empleados.domain.model.Country;

public interface CountryRepository {
	
	List<Country> obtenerPaises(); 

}
