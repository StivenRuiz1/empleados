package com.test.empleados.domain.repository;

import java.util.List;

import com.test.empleados.domain.model.Employee;
import com.test.empleados.domain.model.IdentificationType;

public interface EmployeesRepository {
	
	List<Employee> obtenerEmpleadosFiltrados(String firstName, String otherNames, 
			String surname, String secondSurname, 
			Integer identificationType, String identificationNumber, 
			Integer countryEmployment, String email, String state); 
	
	Employee crearEmpleado(Employee employee);
	
	void eliminarEmpleado(Integer employeeId);
	
	Integer obtenerIdCorreo(String email, String domain);
	
	boolean existeEmpleadoPorIdentificacion(IdentificationType identificationType, String identificationNumber);
	
	boolean existeEmpleadoPorId(Integer id);
	
	Employee obtenerEmpleadoPorId(Integer id);
	
}
