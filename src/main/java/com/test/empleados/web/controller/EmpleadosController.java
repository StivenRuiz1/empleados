package com.test.empleados.web.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.test.empleados.domain.DTO.response.ResponseDTO;
import com.test.empleados.domain.model.Employee;
import com.test.empleados.domain.service.EmployeesService;
import com.test.empleados.domain.service.utils.Utils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/empleado")
@Api(value = "Controlador que contiene los métodos del API relacionados con las actividades")
@ApiResponses(value = { @ApiResponse(code = 200, message = "Respuesta exitosa."),
		@ApiResponse(code = 400, message = "Petición incorrecta."),
		@ApiResponse(code = 404, message = "No se encontraron datos."),
		@ApiResponse(code = 500, message = "Error del servidor.") })
public class EmpleadosController {

	@Autowired
	EmployeesService employeesService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpleadosController.class);
	
	@ApiOperation(value = "Consulta los empleados", notes = "Permite consultar los empleados")
	@GetMapping(value = "/empleados", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getActivities(
	@ApiParam(value = "Página actual del paginador.", required = true) 
	@RequestParam(value = "currentPage", defaultValue = "1") Integer currentPage,
	@ApiParam(value = "Registros por página del paginador.", required = true) 
	@RequestParam(value = "perPage", defaultValue = "10") Integer perPage,
	@ApiParam(value = "Primer nombre que se usara para filtrar la consulta.", required = false) 
	@RequestParam(value = "firstName",  defaultValue = "0") String firstName,
	@ApiParam(value = "Segundo nombre que se usara para filtrar la consulta.", required = false) 
	@RequestParam(value = "otherNames", defaultValue = "0") String otherNames,
	@ApiParam(value = "Primer apellido que se usara para filtrar la consulta.", required = false) 
	@RequestParam(value = "surname",  defaultValue = "0") String surname,
	@ApiParam(value = "Segundo apellido que se usara para filtrar la consulta.", required = false) 
	@RequestParam(value = "secondSurname", defaultValue = "0") String secondSurname,
	@ApiParam(value = "Tipo Identificacion que se usara para filtrar la consulta.", required = false) 
	@RequestParam(value = "identificationType", defaultValue = "0") Integer identificationType,
	@ApiParam(value = "Numero Identificacion que se usara para filtrar la consulta.", required = false) 
	@RequestParam(value = "identificationNumber", defaultValue = "0") String identificationNumber,
	@ApiParam(value = "Pais del empleo que se usara para filtrar la consulta.", required = false) 
	@RequestParam(value = "countryEmployment", defaultValue = "0") Integer countryEmployment,
	@ApiParam(value = "Email que se usara para filtrar la consulta.", required = false) 
	@RequestParam(value = "email", defaultValue = "0") String email,
	@ApiParam(value = "Estado para filtrar la consulta", required = false)
	@RequestParam(value = "state", defaultValue = "0") String state) {

		ResponseDTO response;
		try {
			response = employeesService.obtenerEmpleados(currentPage, perPage, 
					firstName, otherNames, surname, secondSurname, identificationType, 
					identificationNumber, countryEmployment, email, state);
			
			if(response.getStatus() == HttpStatus.NOT_FOUND.value()) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
			} else if (response.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
				
			return ResponseEntity.status(HttpStatus.OK).body(response);
			
		} catch (Exception ex) {
			response = Utils.errorResponse(LOGGER, ex);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(response);
		}
	}
	
	@ApiOperation(value = "Crear empleado", notes = "Permite crear el empleado")
	@PostMapping(value = "/empleado", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> saveEmployee(@Valid @RequestBody Employee employee) {

		ResponseDTO response;
		try {
			response = employeesService.crearEmpleado(employee);
			
			if(response.getStatus() == HttpStatus.BAD_REQUEST.value()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			} else if (response.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			} else if (response.getStatus() == HttpStatus.OK.value()) {
				return ResponseEntity.status(HttpStatus.OK).body(response);
			}
				
			return ResponseEntity.status(HttpStatus.CREATED).body(response);
			
		} catch (Exception ex) {
			response = Utils.errorResponse(LOGGER, ex);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(response);
		}
	}
	
	@ApiOperation(value = "Actualizar empleado", notes = "Permite actualizar el empleado")
	@PutMapping(value = "/empleado/{employeeId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> updateEmployee(
			@PathVariable(name = "employeeId") Integer employeeId,
			@Valid @RequestBody Employee employee) {

		ResponseDTO response;
		try {
			employee.setEmployeeId(employeeId);
			response = employeesService.actualizarEmpleado(employee);
			
			if(response.getStatus() == HttpStatus.BAD_REQUEST.value()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			} else if(response.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			} else if(response.getStatus() == HttpStatus.NOT_FOUND.value()) {
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
			}
			
				
			return ResponseEntity.status(HttpStatus.OK).body(response);
			
		} catch (Exception ex) {
			response = Utils.errorResponse(LOGGER, ex);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(response);
		}
	}
	
	@ApiOperation(value = "Eliminar empleado", notes = "Permite eliminar el empleado")
	@DeleteMapping(value = "/empleado/{employeeId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> deleteEmployee(
			@PathVariable(name = "employeeId") Integer employeeId) {

		ResponseDTO response;
		try {
			response = employeesService.eliminarEmpleado(employeeId);
			
			if(response.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
				
			return ResponseEntity.status(HttpStatus.OK).body(response);
			
		} catch (Exception ex) {
			response = Utils.errorResponse(LOGGER, ex);
			return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(response);
		}
	}
}
